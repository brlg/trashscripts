#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.request, urllib.error, urllib.parse
import re, time, sys,os
import base64

auth_token = ""
auth_key = "bcd151073c03b352e1ef2fd66c32209da9ca0afa"


def auth1():
    url = "https://radiko.jp/v2/api/auth1"
    auth_response = {}

    headers = {
        "User-Agent": "curl/7.56.1",
        "Accept": "*/*",
        "X-Radiko-App": "pc_html5",
        "X-Radiko-App-Version": "0.0.1",
        "X-Radiko-User": "dummy_user",
        "X-Radiko-Device": "pc",
    }
    req = urllib.request.Request(url, None, headers)
    res = urllib.request.urlopen(req)
    auth_response["body"] = res.read()
    auth_response["headers"] = res.info()

    return auth_response


def get_partial_key(auth_response):
    authtoken = auth_response["headers"]["x-radiko-authtoken"]
    offset = auth_response["headers"]["x-radiko-keyoffset"]
    length = auth_response["headers"]["x-radiko-keylength"]

    offset = int(offset)
    length = int(length)
    partialkey = auth_key[offset:offset + length]
    partialkey = base64.b64encode(partialkey.encode())

    return [partialkey, authtoken]


def auth2(partialkey, auth_token):
    url = "https://radiko.jp/v2/api/auth2"
    headers = {
        "X-Radiko-AuthToken": auth_token,
        "X-Radiko-Partialkey": partialkey,
        "X-Radiko-User": "dummy_user",
        "X-Radiko-Device": 'pc'
    }
    req = urllib.request.Request(url, None, headers)
    res = urllib.request.urlopen(req)

    txt = res.read()
    area = txt.decode()
    return area


def gen_temp_chunk_m3u8_url(url, auth_token):
    headers = {
        "X-Radiko-AuthToken": auth_token,
    }

    req = urllib.request.Request(url, None, headers)
    res = urllib.request.urlopen(req)

    body = res.read().decode()
    lines = re.findall('^https?://.+m3u8$', body, flags=(re.MULTILINE))

    return lines[0]


res = auth1()
ret = get_partial_key(res)
token = ret[1]
partialkey = ret[0]
auth2(partialkey, token)
#url = 'https://radiko.jp/v2/api/ts/playlist.m3u8?station_id=QRR&l=15&ft=20171231210000&to=20171231213000&seek=20171231210053'
url = 'http://f-radiko.smartstream.ne.jp/QRR/_definst_/simul-stream.stream/playlist.m3u8'
if(len(sys.argv)>1):
    url = sys.argv[1]
m3u8 = gen_temp_chunk_m3u8_url(url, token)
data = {'token': token, 'url': m3u8}
curDate = time.strftime("%Y%m%d%H%M%S", time.localtime())
print("ffmpeg -headers 'X-Radiko-Authtoken:" + str(data['token']) + "' -i \"" + str(data['url']) + "\" -acodec copy -vn " + curDate + ".aac")
os.system("ffmpeg -i \"" + str(data['url']) + "\" -acodec copy -vn " + curDate + ".aac")
